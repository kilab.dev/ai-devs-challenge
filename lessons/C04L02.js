import {submitAnswer} from "../common/submitAnswer.js"
import {getToken} from "../common/getToken.mjs"
import {getTask} from "../common/getTask.mjs";
import OpenAI from "openai";

const openai = new OpenAI()
const taskName = "tools"
const tokenResponse = await getToken(taskName)
const taskResponse = await getTask(tokenResponse.token)

const aiResponse = await openai.chat.completions.create({
  messages: [
    {
      role: 'system',
      content: `
      Decide whether the task should be added to the ToDo list or to the calendar (if time is provided) and return the corresponding JSON.
      
      Examples:
      Input: Przypomnij mi, że mam kupić mleko
      Output:  {"tool":"ToDo","desc":"Kup mleko" }
      
      Input: Jutro mam spotkanie z Marianem
      Output: {"tool":"Calendar","desc":"Spotkanie z Marianem","date":"2023-11-18"}

      ###      

      Today is 2023-11-17. Always use YYYY-MM-DD format for dates. 
      `,
    },
    {
      role: 'user',
      content: `${taskResponse.question}`,
    },
  ],
  model: process.env.OPENAI_MODEL_GPT_4,
})

// Pobranie tokena z odpowiedzi
const token = tokenResponse.token

// Stworzenie odpowiedzi
const answer = JSON.parse(aiResponse.choices[0].message.content)

// Zgłoszenie odpowiedzi
await submitAnswer(token, { answer })
