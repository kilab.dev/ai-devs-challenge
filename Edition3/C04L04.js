import express from 'express'
import OpenAI from 'openai'
import {config} from '../common/config.mjs'

const openai = new OpenAI()
const app = express()

app.use(express.json())

app.post('/', async (req, res) => {
  try {
    const {instruction} = req.body

    if (!instruction) {
      return res.status(400).json({error: 'Missing instruction field'})
    }

    console.log('Instruction:', instruction)

    const completion = await openai.chat.completions.create({
      messages: [
        {
          role: 'system',
          content: `\`Jesteś asystentem nawigującym po mapie 4x4.
      
          ZASADY PORUSZANIA SIĘ:
          1. Zawsze zaczynasz z pola A1 (Start)
          2. Resetowanie ruchu:
             - Słowa "nie", "czekaj", "nowa", "nowe" - zignoruj wszystko przed nimi i zacznij od nowa z A1
          
          3. Komendy ruchu (wykonuj w kolejności podanej w instrukcji):
             - W DÓŁ/NA DÓŁ/MAKSYMALNIE W DÓŁ/NA SAM DÓŁ/LECIMY W DÓŁ:
               * Przesuń się do ostatniego pola w aktualnej kolumnie (wiersz 4)
             - W PRAWO/NA PRAWO/MAKSYMALNIE W PRAWO:
               * Przesuń się do ostatniego pola w aktualnym wierszu (kolumna D)
             - "O DWA POLA" + kierunek:
               * Przesuń się o 2 pola we wskazanym kierunku (jeśli możliwe)
          
          4. Ignoruj słowa ozdobne jak "dobra", "to co", "zaczynamy", "czas na", "lecimy", itp.
          
          ODPOWIEDŹ:
          - Podaj wyłącznie zawartość końcowego pola (1-2 słowa)
          
          MAPA:
          A1: Start    B1: Trawa    C1: Drzewo   D1: Dom
          A2: Trawa    B2: Wiatrak   C2: Trawa    D2: Trawa
          A3: Trawa    B3: Trawa     C3: Skały    D3: Drzewa
          A4: Góry     B4: Góry      C4: Samochód D4: Jaskinia
          `,
        },
        {
          role: 'user',
          content: instruction,
        },
      ],
      model: process.env.OPENAI_MODEL_GPT_4,
    })

    const description = completion.choices[0].message.content

    console.log('Description:', description)

    res.json({description})

  } catch (error) {
    console.error('Error:', error)
    res.status(500).json({error: 'Internal server error'})
  }
})

const PORT = process.env.PORT || 3000
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`)
})

const requestData = {
  task: 'webhook',
  apikey: config.apiKey,
  answer: 'https://azyl-50379.ag3nts.org/'
}

console.log(requestData)

const verifyResponse = await fetch('https://centrala.ag3nts.org/report', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(requestData),
})

console.log('Result:', await verifyResponse.json())
