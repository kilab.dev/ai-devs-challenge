import OpenAI from 'openai'

import {config} from '../common/config.mjs'

const openai = new OpenAI()

const dataForCensoring = await fetch(`https://centrala.ag3nts.org/data/${config.apiKey}/cenzura.txt`).then(res => res.text())

console.log(`Data for censoring: ${dataForCensoring}`)

const chatCompletion = await openai.chat.completions.create({
  messages: [
    {
      role: 'system',
      content: `
        As an assistant, You have to censor the sensitive data. Do not censor word: "lat".
        Sensible data is words like: "name with surname", "city", "street name with home number", "number of years lived".
        Replace censored word with "CENZURA".
        Do not change the order of the words and do not remove any punctuation. 
        
        Sentence to censor: Osoba: Paweł Balicki, zamieszkały w Gdańsku przy ul. Dywizjonu 303 3E, wiek 44 lat.
      `,
    },
    {
      role: 'user',
      content: dataForCensoring,
    },
  ],
  model: process.env.OPENAI_MODEL_GPT_4,
})

const answer = chatCompletion.choices[0].message.content

console.log(`Data after censoring: ${answer}`)
console.log('========')

const requestData = {
  task: 'CENZURA',
  apikey: config.apiKey,
  answer: answer
}

const verifyResponse = await fetch('https://centrala.ag3nts.org/report', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(requestData),
})

const result = await verifyResponse.json()
console.log('Success:', result)
