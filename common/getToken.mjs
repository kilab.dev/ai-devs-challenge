import {config} from "./config.mjs";

export async function getToken(taskName) {
  const url = `${config.taskManagerUrl}/token/${taskName}`;
  const response = await fetch(url, {
    method: 'POST',
    body: JSON.stringify({ apikey: config.apiKey })
  });

  return await response.json();
}
