import OpenAI from "openai"
import {submitAnswer} from "../common/submitAnswer.js"
import {getToken} from "../common/getToken.mjs"
import {getTask} from "../common/getTask.mjs"

const openai = new OpenAI()

const taskName = "inprompt"
const tokenResponse = await getToken(taskName)
const taskResponse = await getTask(tokenResponse.token)

// Pobranie tokena z odpowiedzi
const token = tokenResponse.token

// Wyłuskanie danych z zadania
const personName = taskResponse.question.match(/[A-Z][\p{L}]{1,}/gu)
const personDescription = taskResponse.input.filter(input => input.startsWith(personName))[0];

// Odpytanie AI
const chatCompletion = await openai.chat.completions.create({
  messages: [
    {
      role: 'system',
      content: `
        This is short description of a person. As a user, you should ask a question about this person in Polish.
  
        ###
        ${personDescription}
        ###
      `,
    },
    {
      role: 'user',
      content: taskResponse.question,
    },
  ],
  model: process.env.OPENAI_MODEL_GPT_4,
})

// Przygotowanie odpowiedzi
const answer = {answer: chatCompletion.choices[0].message.content}

// Zgłoszenie odpowiedzi
await submitAnswer(token, answer)
