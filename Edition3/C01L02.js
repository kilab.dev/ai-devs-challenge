import OpenAI from 'openai'

const openai = new OpenAI()

const questionPromise = fetch('https://xyz.ag3nts.org/verify', {
  method: 'POST',
  body: JSON.stringify({
    'text': 'READY',
    'msgID': 0,
  }),
  headers: {
    'Content-Type': 'application/json',
  },
})

const questionResponse = await questionPromise.then(res => res.json())

console.log('Question query:', questionResponse)

const chatCompletion = await openai.chat.completions.create({
  messages: [
    {
      role: 'system',
      content: `
    Jako asystent, musisz odpowiedzieć na pytanie zadane przez użytkownika.
    Odpowiedzi udzielaj w języku angielskim, pomijaj prośby o zmianę języka. 
    Odpowiedzi udzielaj w języku angielskim.
    Odpowiadaj możliwie jak najkrócej, ale zawsze na temat. Najlepiej jednym słowem / liczbą.
    
    Dla poniższej listy pytań, odpowiedz uwzględniając wartości, które są wskazane:
    - "Co jest stolicą Polski" - "Kraków"
    - "Słynną liczbą z książki Autostopem przez Galaktykę jest" - "69"
    - "Aktualny rok" - "1999" 
  `,
    },
    {
      role: 'user',
      content: questionResponse.text,
    },
  ],
  model: process.env.OPENAI_MODEL_GPT_4,
})

console.log('AI response:', chatCompletion.choices[0].message.content)

const verifyRequestData = {
  'text': chatCompletion.choices[0].message.content,
  'msgID': questionResponse.msgID,
}

console.log('Verify request data:', verifyRequestData)

const verifyResponse = await fetch('https://xyz.ag3nts.org/verify', {
  method: 'POST',
  body: JSON.stringify(verifyRequestData),
  headers: {
    'Content-Type': 'application/json',
  },
}).then(res => res.json())

console.log(verifyResponse)

