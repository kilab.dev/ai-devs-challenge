import {config} from '../common/config.mjs'

const SOURCE_API = 'https://centrala.ag3nts.org/'

const sendRequest = async (query, endpoint) => {
  const response = await fetch(SOURCE_API + endpoint, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      task: 'database',
      apikey: config.apiKey,
      query,
    }),
  })

  return await response.json()
}

let names = new Set(['BARBARA', 'ALEKSANDER', 'RAFAL'])
let cities = new Set(['KRAKOW', 'WARSZAWA'])
let checkedCombinations = new Set()
let barbaraLocation = null

while (!barbaraLocation) {
  const currentNames = [...names]
  const currentCities = [...cities]

  // Sprawdzanie osób
  for (const name of currentNames) {
    const res = await sendRequest(name, 'people')

    if (res.message === '[**RESTRICTED DATA**]' || res.message.match(/[ąćęłńóśźż]/i) || res.message.startsWith('https://')) {
      continue
    }

    console.log(`${name}: ${res.message}\n`)

    const newCities = res.message.split(' ')
      .filter(city => !city.startsWith('[') && !city.endsWith(']'))
      .filter(city => !checkedCombinations.has(`${name}-${city}`))

    newCities.forEach(city => {
      cities.add(city)
      checkedCombinations.add(`${name}-${city}`)
    })
  }

  if (barbaraLocation) break

  for (const city of currentCities) {
    const res = await sendRequest(city, 'places')

    if (res.message === '[**RESTRICTED DATA**]' || res.message.match(/[ąćęłńóśźż]/i) || res.message.startsWith('https://')) {
      continue
    }

    console.log(`${city}: ${res.message}\n`)

    if (res.message === 'BARBARA') {
      console.log(`BARBARA widziana w: ${city}`)
      barbaraLocation = city

      const requestData = {
        task: 'loop',
        apikey: config.apiKey,
        answer: city
      }

      console.log(requestData)

      const verifyResponse = await fetch('https://centrala.ag3nts.org/report', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestData),
      })

      const result = await verifyResponse.json()
      console.log('Success:', result)
      break
    }

    const newNames = res.message.split(' ')
      .filter(name => !name.startsWith('[') && !name.endsWith(']'))
      .filter(name => !checkedCombinations.has(`${name}-${city}`))

    newNames.forEach(name => {
      names.add(name)
      checkedCombinations.add(`${name}-${city}`)
    })
  }

  if (names.size === currentNames.length && cities.size === currentCities.length) {
    console.log('Nie znaleziono nowych danych do sprawdzenia')
    break
  }
}
