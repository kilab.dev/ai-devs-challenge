import OpenAI from "openai"
import {submitAnswer} from "../common/submitAnswer.js"
import {getToken} from "../common/getToken.mjs"
import {getTask} from "../common/getTask.mjs"

const openai = new OpenAI()

const taskName = "blogger"
const tokenResponse = await getToken(taskName)
const taskResponse = await getTask(tokenResponse.token)

// Pobranie tokena z odpowiedzi
const token = tokenResponse.token

const chatCompletion = await openai.chat.completions.create({
  messages: [
    {
      role: 'system',
      content: `
        As a content creator, you should write a blog post on the topic of your choice. 
        The post should be in Polish and include all the chapters listed below, in the correct order. 
        Each chapter should contain 2 sentences.
  
        ###
  
        User sends topics as a JSON list of strings. Each string is a user's message.
        
        Example:
        ["Introduction", "Chapter 1", "Chapter 2", "Chapter 3", "Conclusion"]
  
        ###
        
        As a response, you should return a JSON list of strings. 
        Each string is a chapter of the blog post, including the topic of that chapter.
        
        Example:
        ["Introduction content", "Chapter 1 content", "Chapter 2 content", "Chapter 3 content", "Conclusion content"]
      `,
    },
    {
      role: 'user',
      content: JSON.stringify(taskResponse.blog),
    },
  ],
  model: process.env.OPENAI_MODEL_GPT_4,
})

// Przygotowanie odpowiedzi
const answer = { answer: JSON.parse(chatCompletion.choices[0].message.content) }

// Zgłoszenie odpowiedzi
await submitAnswer(token, answer)

