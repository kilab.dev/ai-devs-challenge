import {config} from '../common/config.mjs'
import OpenAI from 'openai'

const openai = new OpenAI()

const sendApiRequest = async (query) => {
  const requestBody = {
    task: 'photos',
    apikey: config.apiKey,
    answer: query,
  }
  console.debug('Sending request:', requestBody)
  const response = await fetch('https://centrala.ag3nts.org/report', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(requestBody),
  })

  return await response.json()
}

const initialResponse = await sendApiRequest('START')
console.log('Initial response:', initialResponse.message)

// fetch photos from message
const photoLinksResponse = await openai.chat.completions.create({
  model: process.env.OPENAI_MODEL_GPT_4O_MINI,
  messages: [
    {
      role: 'system',
      content: `You are a assistant that helps people to find photos URL from a message.
      You should provide the URL of the photos in the message as a JSON list without any additional characters: [{"url": "URL_1", fileName:"IMG123.PNG"}, {"url": "URL_2", fileName:"IMG123.PNG"}].
      `,
    },
    {
      role: 'user',
      content: initialResponse.message,
    },
  ],
})

const photosLinks = JSON.parse(photoLinksResponse.choices[0].message.content)
const fixedPhotos = []
console.log('Fetched photos links:', photosLinks)

for (const photoLink of photosLinks) {
  const photoActionResponse = await openai.chat.completions.create({
    model: process.env.OPENAI_MODEL_GPT_4O,
    messages: [
      {
        role: 'system',
        content: `You are a assistant that helps find operations to be performed on photos.
          If photo is fine, respond with "SKIP".
          Available operations are: "REPAIR", "DARKEN", "BRIGHTEN".
          Reply only with the operation name single word.
      `,
      },
      {
        role: 'user',
        content: [
          {
            type: 'text',
            text: 'Which action should I perform on the photos?',
          },
          {
            type: "image_url",
            image_url: {
              url: photoLink.url,
              detail: "high"
            }
          },
        ],
      },
    ],
  })

  const photoAction = photoActionResponse.choices[0].message.content
  console.log('Photo action:', photoAction)

  if (photoAction === 'SKIP') {
    console.log('Skipping photo')
    continue
  }

  const fixPhotoResponse = await sendApiRequest(`${photoAction} ${photoLink.fileName}`)

  console.log('Fix photo response:', fixPhotoResponse)

  try {
    let fixedPhoto = fixPhotoResponse.message.match(/IMG_\d+_\w+\.PNG/g)[0]
      .replace('https://centrala.ag3nts.org/dane/barbara/', '')
    fixedPhoto = `https://centrala.ag3nts.org/dane/barbara/${fixedPhoto}`
    fixedPhotos.push(fixedPhoto)
    console.log('Fixed photo:', fixedPhoto)

    const describeWomanResponse = await openai.chat.completions.create({
      model: process.env.OPENAI_MODEL_GPT_4O_MINI,
      messages: [
        {
          role: 'system',
          content: `Jesteś asystentem, który pomaga opisać kobietę na zdjęciu. 
            Podaj rysopis jednej kobiety na zdjęciu. Zawrzyj informację o:
            - kolorze włosów,
            - znakach charakterystycznych,
            Pomiń opis otoczenia.
          `,
        },
        {
          role: 'user',
          content: [
            {
              type: 'text',
              text: 'Opisz kobietę na zdjęciu',
            },
            {
              type: "image_url",
              image_url: {
                url: fixedPhoto,
                detail: "high"
              }
            },
          ],
        },
      ],
    })

    console.log('Described woman:', describeWomanResponse.choices[0].message.content)

    const taskAnswer = await sendApiRequest(describeWomanResponse.choices[0].message.content)
    console.log('Task answer:', taskAnswer)

    console.log('===================')
  } catch (error) {

  }
}
