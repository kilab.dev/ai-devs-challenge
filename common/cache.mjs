import fs from 'fs/promises'
import crypto from 'crypto'
import path from 'path'

export async function ensureCacheDir(cacheDir) {
  try {
    await fs.mkdir(cacheDir, {recursive: true})
  } catch (err) {
    if (err.code !== 'EEXIST') throw err
  }
}

export function getCacheKey(cacheKey) {
  return crypto
    .createHash('md5')
    .update(cacheKey)
    .digest('hex')
}

export async function getFromCache(cacheDir, cacheKey) {
  try {
    const data = await fs.readFile(path.join(cacheDir, cacheKey), 'utf-8')
    return JSON.parse(data)
  } catch {
    return null
  }
}

export async function saveToCache(cacheDir, cacheKey, data) {
  await fs.writeFile(
    path.join(cacheDir, cacheKey),
    JSON.stringify(data),
    'utf-8'
  )
}
