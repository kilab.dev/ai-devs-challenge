import OpenAI from "openai"
import {submitAnswer} from "../common/submitAnswer.js"
import {getToken} from "../common/getToken.mjs"

const openai = new OpenAI()

const taskName = "embedding"
const tokenResponse = await getToken(taskName)

// Pobranie tokena z odpowiedzi
const token = tokenResponse.token

// Odpytanie AI
const embeddingResponse = await openai.embeddings.create({
  input: 'Hawaiian pizza',
  model: process.env.OPENAI_MODEL_ADA_002
})

// Przygotowanie odpowiedzi
const answer = {answer: embeddingResponse.data[0].embedding}

// Zgłoszenie odpowiedzi
await submitAnswer(token, answer)
