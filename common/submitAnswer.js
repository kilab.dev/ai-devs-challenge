import {config} from "./config.mjs";

export async function submitAnswer(token, answer) {
  const url = `${config.taskManagerUrl}/answer/${token}`;
  const response = await fetch(url, {
    method: 'POST',
    body: JSON.stringify(answer),
    headers: {'Content-Type': 'application/json'},
  });

  const parsedResponse = await response.json();

  if (parsedResponse.code === 0) {
    console.log("Zadanie zaliczone!")
  } else {
    console.error(`Błąd podczas zgłaszania odpowiedzi: ${parsedResponse.msg}`)
  }
}
