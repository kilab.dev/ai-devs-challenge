import {submitAnswer} from "../common/submitAnswer.js"
import {getToken} from "../common/getToken.mjs"
import {getTask} from "../common/getTask.mjs";
import OpenAI from "openai";

const openai = new OpenAI()
const taskName = "ownapi"
const tokenResponse = await getToken(taskName)
const taskResponse = await getTask(tokenResponse.token)
console.log(taskResponse)

// Pobranie tokena z odpowiedzi
const token = tokenResponse.token

// Stworzenie odpowiedzi
const answer = { answer: 'https://openai.kilab.dev/' }

// Zgłoszenie odpowiedzi
await submitAnswer(token, answer)
