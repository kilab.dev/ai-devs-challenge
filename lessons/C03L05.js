import {submitAnswer} from "../common/submitAnswer.js"
import {getToken} from "../common/getToken.mjs"
import {getTask} from "../common/getTask.mjs";
import OpenAI from "openai";

const openai = new OpenAI()
const taskName = "people"
const tokenResponse = await getToken(taskName)
const taskResponse = await getTask(tokenResponse.token)

const databaseSource = await fetch('https://zadania.aidevs.pl/data/people.json')
const database = await databaseSource.json()
const tempDatabase = {}

database.forEach((element) => {
  tempDatabase[`${element.imie} ${element.nazwisko}`] = element
})

const chatCompletion = await openai.chat.completions.create({
  messages: [
    {
      role: 'system',
      content: `
      From the provided message, find the name and surname of the person. Maximum length for the answer is 200 characters
      
      ### 
      Some examples:
      
      Message: "Jasiek Waligórski jest trenerem w AI Devs."
      Response: "Jan Waligórski"
      
      Message: "Krysia Swoboda jest trenerką w AI Devs."
      Response: "Krystyna Swoboda"
      `,
    },
    {
      role: 'user',
      content: `${taskResponse.question}`,
    },
  ],
  model: process.env.OPENAI_MODEL_GPT_4,
})

const foundPerson = tempDatabase[chatCompletion.choices[0].message.content]

if (foundPerson) {
  const chatCompletion = await openai.chat.completions.create({
    messages: [
      {
        role: 'system',
        content: `
        From the provided context answer the question. Maximum length for the answer is 200 characters. Use Polish lang.
        
        Context:
        - person description: ${foundPerson.o_mnie}
        - age: ${foundPerson.wiek}
        - favourite movie: ${foundPerson.ulubiony_film}
        - favourite color: ${foundPerson.ulubiony_kolor}
      `,
      },
      {
        role: 'user',
        content: `${taskResponse.question}`,
      },
    ],
    model: process.env.OPENAI_MODEL_GPT_4,
  })

  // Stworzenie odpowiedzi
  const answer = {answer: chatCompletion.choices[0].message.content}

  // Pobranie tokena z odpowiedzi
  const token = tokenResponse.token

  // Zgłoszenie odpowiedzi
  await submitAnswer(token, answer)
}
