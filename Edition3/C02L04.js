import OpenAI from 'openai'
import fs from 'fs/promises'
import path from 'path'

import {config} from '../common/config.mjs'

const openai = new OpenAI()

const CACHE_FILE = 'analysis_cache.json'
const DIRECTORY_PATH = 'Edition3/res/C02L04'
const SYSTEM_PROMPT = {
  role: 'system',
  content: `Zdecyduj, czy treść raportu zawiera informacje o schwytanych ludziach lub o naprawionych usterkach sprzętowych.
   Akcje związane z oprogramowaniem, aktualizacją, czy wymianą części nie jest uważane za naprawę sprzętową.
   Jeśli dotyczy schwytanej osoby, odpowiedz "people". 
   Jeśli naprawy sprzętowej maszyny, odpowiedz "hardware". 
   W przeciwnym razie odpowiedz "other". 
   Zanim udzielisz odpowiedzi, przedstaw swój tok myślenia. 
   Odpowiedzi udziel w formacie: answer: people/hardware/other
 `
}

async function loadCache() {
  try {
    const cacheData = await fs.readFile(CACHE_FILE, 'utf-8')
    return JSON.parse(cacheData)
  } catch (error) {
    return {}
  }
}

async function saveCache(cache) {
  await fs.writeFile(CACHE_FILE, JSON.stringify(cache, null, 2))
}

async function analyzeTextFile(filePath, cache) {
  if (cache[filePath]) {
    return cache[filePath]
  }

  const content = await fs.readFile(filePath, 'utf-8')
  const response = await openai.chat.completions.create({
    model: process.env.OPENAI_MODEL_GPT_4O_MINI,
    messages: [
      SYSTEM_PROMPT,
      {
        role: 'user',
        content: content
      },
    ]
  })

  const result = response.choices[0].message.content.toLowerCase()
  const answer = result.split('answer: ')[1]

  cache[filePath] = answer

  await saveCache(cache)

  return answer
}

async function analyzeImageFile(filePath, cache) {
  if (cache[filePath]) {
    return cache[filePath]
  }

  const imageBuffer = await fs.readFile(filePath)
  const base64Image = imageBuffer.toString('base64')
  const response = await openai.chat.completions.create({
    model: process.env.OPENAI_MODEL_GPT_4O_MINI,
    messages: [
      SYSTEM_PROMPT,
      {
        role: 'user',
        content: [
          {
            type: 'image_url',
            image_url: {
              url: `data:image/png;base64,${base64Image}`
            }
          }
        ]
      },
    ]
  })

  const result = response.choices[0].message.content.toLowerCase()
  const answer = result.split('answer: ')[1]

  cache[filePath] = answer

  await saveCache(cache)

  return answer
}

async function analyzeAudioFile(filePath, cache) {
  if (cache[filePath]) {
    return cache[filePath]
  }

  const fileBuffer = await fs.readFile(filePath)
  const audioFile = new File([fileBuffer], path.basename(filePath), {type: 'audio/mpeg'})

  const transcription = await openai.audio.transcriptions.create({
    file: audioFile,
    model: process.env.OPENAI_MODEL_WHISPER
  })

  const response = await openai.chat.completions.create({
    model: process.env.OPENAI_MODEL_GPT_4O_MINI,
    messages: [
      SYSTEM_PROMPT,
      {
        role: 'user',
        content: transcription.text
      },
    ]
  })

  const result = response.choices[0].message.content.toLowerCase()
  const answer = result.split('answer: ')[1]

  cache[filePath] = answer

  await saveCache(cache)

  return answer
}

async function processFiles() {
  try {
    const cache = await loadCache()
    const files = await fs.readdir(DIRECTORY_PATH)

    for (const file of files) {
      const filePath = path.join(DIRECTORY_PATH, file)
      const extension = path.extname(file).toLowerCase()

      let result
      try {
        switch (extension) {
          case '.txt':
            result = await analyzeTextFile(filePath, cache)
            break
          case '.png':
            result = await analyzeImageFile(filePath, cache)
            break
          case '.mp3':
            result = await analyzeAudioFile(filePath, cache)
            break
          default:
            console.log(`Skipping unsupported file: ${file}`)
            continue
        }
        console.log(`${file}: ${result} (${result === cache[filePath] ? 'cached' : 'new'})`)
      } catch (error) {
        console.error(`Error processing ${file}:`, error.message)
      }
    }
  } catch (error) {
    console.error('Error reading directory:', error.message)
  }
}

processFiles()
  .then(async () => {
    console.log('All files processed successfully')

    const requestData = {
      task: 'kategorie',
      apikey: config.apiKey,
      answer: {
        people: [],
        hardware: [],
      }
    }

    const cache = await loadCache()
    for (const [filePath, answer] of Object.entries(cache)) {
      const fileName = path.basename(filePath)
      if (answer === 'people') {
        requestData.answer.people.push(fileName)
      } else if (answer === 'hardware') {
        requestData.answer.hardware.push(fileName)
      }
    }

    const verifyResponse = await fetch('https://centrala.ag3nts.org/report', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestData),
    })

    const result = await verifyResponse.json()
    console.log('Success:', result)

  })
  .catch(e => console.error('Error processing files:', e.message))
