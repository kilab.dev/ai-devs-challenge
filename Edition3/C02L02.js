import OpenAI from 'openai'
import fs from 'fs'

const openai = new OpenAI()

const filePath = `${process.cwd()}/Edition3/res/C02L02.jpg`
const imageBase64 = fs.readFileSync(filePath, 'base64')

const chatCompletion = await openai.chat.completions.create({
  messages: [
    {
      role: "system",
      content: "You are a helpful assistant that can answer questions and help with tasks."
    },
    {
      role: "user",
      content: [
        {
          type: "image_url",
          image_url: {
            url: `data:image/jpeg;base64,${imageBase64}`,
            detail: "high"
          }
        },
        {
          type: "text",
          text: "Zanalizuj przesłaną grafikę"
        },
      ]
    }
  ],
  model: process.env.OPENAI_MODEL_GPT_4O_MINI
})

console.log(chatCompletion.choices[0].message)
