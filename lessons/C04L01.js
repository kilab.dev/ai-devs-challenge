import {submitAnswer} from "../common/submitAnswer.js"
import {getToken} from "../common/getToken.mjs"
import {getTask} from "../common/getTask.mjs";
import OpenAI from "openai";

const openai = new OpenAI()
const taskName = "knowledge"
const tokenResponse = await getToken(taskName)
const taskResponse = await getTask(tokenResponse.token)

const knowledgeSources = {
  '1': {
    scope: 'Currency',
    url: 'https://api.nbp.pl/api/exchangerates/rates/a/$code/?format=json',
  },
  '2': {
    scope: 'Knowledge about countries',
    url: `https://restcountries.com/v3.1/name/$country`,
  }
}

const defineKnowledgeSource = await openai.chat.completions.create({
  messages: [
    {
      role: 'system',
      content: `
      Based on user input in Polish language, define the source of knowledge and get information from question. Return response in format:
       
      {source: 1, dataField: "..."}
      
      If user question is not recognized, answer on question and return response in format:
      
      {source: null, dataField: "..."}
      
      
      Examples:
      Question: "jaki jest teraz kurs dolara?"
      Response: {"source": 1, "data": "USD"}
      
      Question: "jaka jest populacja Niemiec?"
      Response: {"source": 2, "data": "germany"}
            
      Question: "kto napisał Romeo i Julię?"
      Response: {"source": null, "data": "William Shakespeare"}
      
      Available sources:
      1 - ${knowledgeSources['1'].scope}, return currency code as XXX
      2 - ${knowledgeSources['2'].scope}, return country name in English and lowercase
      `,
    },
    {
      role: 'user',
      content: `${taskResponse.question}`,
    },
  ],
  model: process.env.OPENAI_MODEL_GPT_4,
})

const getCurrency = async (currencyCode) => {
  const currencySource = await fetch(knowledgeSources[1].url.replace('$code', currencyCode.toLowerCase()))
  const currencyData = await currencySource.json()

  return currencyData.rates[0].mid
}

const getCountryPopulation = async (countryName) => {
  const countrySource = await fetch(knowledgeSources[2].url.replace('$country', countryName.toLowerCase()))
  const countryData = await countrySource.json()

  return countryData[0].population
}

const knowledgeSource = JSON.parse(defineKnowledgeSource.choices[0].message.content)

const answer = async () => {
  if (knowledgeSource.source === 1) {
    return await getCurrency(knowledgeSource.data)
  } else if(knowledgeSource.source === 2) {
    return await getCountryPopulation(knowledgeSource.data)
  } else {
    return knowledgeSource.data
  }
}

// Pobranie tokena z odpowiedzi
const token = tokenResponse.token

// Zgłoszenie odpowiedzi
await submitAnswer(token, { answer: await answer() })
