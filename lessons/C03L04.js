import {submitAnswer} from "../common/submitAnswer.js"
import {getToken} from "../common/getToken.mjs"
import {getTask} from "../common/getTask.mjs";
import OpenAI from "openai";
import {QdrantClient} from '@qdrant/js-client-rest'

const COLLECTION_NAME = "ai_devs_c3l4"

const openai = new OpenAI()
const qdrant = new QdrantClient({url: process.env.QDRANT_URL});
const taskName = "search"
const tokenResponse = await getToken(taskName)
const taskResponse = await getTask(tokenResponse.token)

// Pobranie tokena z odpowiedzi
const token = tokenResponse.token

// Pobranie embeddingu z zapytania
const queryEmbedding = await openai.embeddings.create({
  input: taskResponse.question,
  model: process.env.OPENAI_MODEL_ADA_002
})

// Zapytanie do Qdrant
const search = await qdrant.search(COLLECTION_NAME, {
  vector: queryEmbedding.data[0].embedding,
  limit: 1,
  filter: {
    must: [
      {
        key: 'source',
        match: {
          value: COLLECTION_NAME
        }
      }
    ]
  }
})

// Stworzenie odpowiedzi
const answer = {answer: search[0].payload.url}

// Zgłoszenie odpowiedzi
await submitAnswer(token, answer)
