import OpenAI from 'openai'
import {config} from '../common/config.mjs'
import {JSDOM} from 'jsdom'

const openai = new OpenAI()

class WebCrawler {
  constructor() {
    this.visitedUrls = new Set()
    this.pageMap = new Map()
    this.baseUrl = 'https://softo.ag3nts.org'
  }

  async crawlPage(url) {
    if (this.visitedUrls.has(url)) {
      return this.pageMap.get(url)
    }

    const response = await fetch(url)
    const html = await response.text()

    this.visitedUrls.add(url)
    this.pageMap.set(url, html)

    return html
  }

  extractLinks(html) {
    const dom = new JSDOM(html)
    return [...dom.window.document.querySelectorAll('a')]
      .map(a => {
        const href = a.href
        if (href.startsWith('/')) {
          return `${this.baseUrl}${href}`
        }
        return href
      })
      .filter(href => href.startsWith(this.baseUrl))
  }

  async analyzeContent(question, content) {
    const completion = await openai.chat.completions.create({
      model: process.env.OPENAI_MODEL_GPT_4O_MINI,
      messages: [
        {
          role: 'system',
          content: `Analizuj treść strony i znajdź odpowiedź na pytanie. 
          Jeśli odpowiedź nie jest dostępna na tej stronie, odpowiedz 'CONTINUE'. 
          Odpowiadaj krótko i konkretnie, bez zbędnych dodatków. Jeśli to są adresy sieciowe, zwróć tylko adres.
          Pomijaj komentarze w kodzie i inne nieistotne informacje.
          Pomijaj stronę /loop.`
        },
        {
          role: 'user',
          content: `Pytanie: ${question}\nTreść strony: ${content}`
        }
      ],
      temperature: 0.3,
      max_tokens: 150
    })

    return completion.choices[0].message.content
  }

  async findAnswer(question, url, depth = 0) {
    if (depth > 3) return null // zabezpieczenie przed zbyt głębokim zagnieżdżeniem

    console.log(`Sprawdzam stronę: ${url}`)
    const content = await this.crawlPage(url)
    const analysis = await this.analyzeContent(question, content)

    if (analysis !== 'CONTINUE') {
      return analysis
    }

    const links = this.extractLinks(content)
    console.log(`Znalezione linki: ${links.length}`)

    const nextUrlCompletion = await openai.chat.completions.create({
      model: process.env.OPENAI_MODEL_GPT_4O_MINI,
      messages: [
        {
          role: 'system',
          content: 'Wybierz link, który najprawdopodobniej prowadzi do odpowiedzi na pytanie. Zwróć tylko URL.'
        },
        {
          role: 'user',
          content: `Pytanie: ${question}\nDostępne linki: ${JSON.stringify(links)}`
        }
      ],
      temperature: 0.3,
      max_tokens: 150
    })

    const nextUrl = nextUrlCompletion.choices[0].message.content
    if (nextUrl && !this.visitedUrls.has(nextUrl)) {
      return await this.findAnswer(question, nextUrl, depth + 1)
    }

    return null
  }
}

async function main() {
  const questions = await fetch(`https://centrala.ag3nts.org/data/${config.apiKey}/softo.json`)
    .then(res => res.json())

  const crawler = new WebCrawler()
  const answers = {}

  for (const [id, question] of Object.entries(questions)) {
    console.log(`\nSzukam odpowiedzi na pytanie ${id}: ${question}`)
    const answer = await crawler.findAnswer(question, 'https://softo.ag3nts.org/')
    if (answer) {
      answers[id] = answer
      console.log(`Znaleziono odpowiedź: ${answer}`)
    }
  }

  const requestData = {
    task: 'softo',
    apikey: config.apiKey,
    answer: answers
  }

  console.log(requestData)

  const verifyResponse = await fetch('https://centrala.ag3nts.org/report', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(requestData),
  })

  console.log('Result:', await verifyResponse.json())
}

main().catch(console.error)
