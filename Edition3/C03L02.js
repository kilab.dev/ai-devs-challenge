import OpenAI from 'openai'
import fs from 'fs/promises'
import {config} from '../common/config.mjs'
import {ensureCacheDir, getFromCache, saveToCache} from '../common/cache.mjs'

const CACHE_DIR = 'cache/E3C3L2'
const SOURCE_DATA_DIRECTORY_PATH = 'Edition3/res/C03L02'
const openai = new OpenAI()

const weaponDescriptionFiles = await fs.readdir(SOURCE_DATA_DIRECTORY_PATH)
const documents = []

for (const weaponDescriptionFile of weaponDescriptionFiles) {
  const filePath = `${SOURCE_DATA_DIRECTORY_PATH}/${weaponDescriptionFile}`
  const fileContent = await fs.readFile(filePath, 'utf-8')

  documents.push({
    filename: weaponDescriptionFile,
    text: fileContent
  })
}

await ensureCacheDir(CACHE_DIR)

let embeddingData = await getFromCache(CACHE_DIR, 'embedding.json')

if (!embeddingData) {
  const embeddingsResponse = await openai.embeddings.create({
    model: process.env.OPENAI_MODEL_EMBEDDING_3_SMALL,
    input: documents.map(doc => doc.text),
    encoding_format: 'float'
  })

  embeddingsResponse.data = embeddingsResponse.data.map((embedding, index) => ({
    ...embedding,
    filename: documents[index].filename
  }))

  await saveToCache(CACHE_DIR, 'embedding.json', embeddingsResponse)
  embeddingData = embeddingsResponse
}

const queryResponse = await openai.embeddings.create({
  model: process.env.OPENAI_MODEL_EMBEDDING_3_SMALL,
  input: 'kradzież prototypu broni',
  encoding_format: 'float'
})

function cosineSimilarity(vecA, vecB) {
  const dotProduct = vecA.reduce((sum, a, i) => sum + a * vecB[i], 0)
  const magnitudeA = Math.sqrt(vecA.reduce((sum, a) => sum + a * a, 0))
  const magnitudeB = Math.sqrt(vecB.reduce((sum, b) => sum + b * b, 0))
  return dotProduct / (magnitudeA * magnitudeB)
}

const queryEmbedding = queryResponse.data[0].embedding
const similarities = embeddingData.data.map(doc => ({
  filename: doc.filename,
  similarity: cosineSimilarity(queryEmbedding, doc.embedding)
}))

similarities.sort((a, b) => b.similarity - a.similarity)

const bestMatch = similarities[0]
const matchingDocument = documents.find(doc => doc.filename === bestMatch.filename)
console.log('Najbardziej pasujący dokument:', bestMatch.filename)

// format filename to YYYY-MM-DD format
const taskAnswer = bestMatch.filename
.replace('.txt', '')
.replace(/_/g, '-')

const requestData = {
  task: 'wektory',
  apikey: config.apiKey,
  answer: taskAnswer
}

console.log(requestData)

const verifyResponse = await fetch('https://centrala.ag3nts.org/report', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(requestData),
})

const result = await verifyResponse.json()
console.log('Success:', result)
