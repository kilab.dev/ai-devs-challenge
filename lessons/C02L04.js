import OpenAI from "openai"
import {submitAnswer} from "../common/submitAnswer.js"
import {getToken} from "../common/getToken.mjs"
import {getTask} from "../common/getTask.mjs"
import * as fs from "fs"

const openai = new OpenAI()

const taskName = "whisper"
const tokenResponse = await getToken(taskName)
const taskResponse = await getTask(tokenResponse.token)

console.log(taskResponse)

// Pobranie tokena z odpowiedzi
const token = tokenResponse.token

// Odpytanie AI
const embeddingResponse = await openai.audio.transcriptions.create({
  file: fs.createReadStream(process.cwd() + '/lessons/res/C02L04.mp3'),
  model: process.env.OPENAI_MODEL_WHISPER
})

// Przygotowanie odpowiedzi
const answer = {answer: embeddingResponse.text}

// Zgłoszenie odpowiedzi
await submitAnswer(token, answer)
