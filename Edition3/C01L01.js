import OpenAI from 'openai'

const openai = new OpenAI()

const htmlContent = await fetch('https://xyz.ag3nts.org/').then(res => res.text())
const question = htmlContent.match(/<p id="human-question">(.*?)<\/p>/)[1].replace('Question:<br />', '').trim()

console.log(`Question: ${question}`)

const chatCompletion = await openai.chat.completions.create({
  messages: [
    {
      role: 'system',
      content: `
        As a assistant, You have to respond with valid answer to the question asked by the user.
        You have to respond with only year in the answer.
      `,
    },
    {
      role: 'user',
      content: question,
    },
  ],
  model: process.env.OPENAI_MODEL_GPT_4O_MINI,
})

const answer = chatCompletion.choices[0].message.content

console.log(`Answer: ${answer}`)
console.log('========')

fetch('https://xyz.ag3nts.org', {
  method: 'POST',
  body: 'username=tester&password=574e112a&answer=' + encodeURIComponent(answer),
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
  },
}).then(r => r.text()).then(console.log)

