import {submitAnswer} from "../common/submitAnswer.js"
import {getToken} from "../common/getToken.mjs"
import {getTask} from "../common/getTask.mjs";
import OpenAI from "openai";

const openai = new OpenAI()

const taskName = 'whoami'
const tokenResponse = await getToken(taskName)
const taskResponse = await getTask(tokenResponse.token)

const hints = [
  taskResponse.hint
]

async function queryAIAndSubmitAnswer(hints) {
  const chatCompletion = await openai.chat.completions.create({
    messages: [
      {
        role: 'system',
        content: `
          Based on the hints provided, guess who the person described is. If you know the answer, please submit it. If not, please submit only "NO".
        `,
      },
      {
        role: 'user',
        content: `Hints: ${hints.join(', ')}`,
      },
    ],
    model: process.env.OPENAI_MODEL_GPT_4,
  })

  if (chatCompletion.choices[0].message.content !== 'NO') {
    const refreshedTokenResponse = await getToken(taskName)
    const answer = {answer: chatCompletion.choices[0].message.content}
    await submitAnswer(refreshedTokenResponse.token, answer)
    return true
  }

  return false
}

let answerSubmitted = await queryAIAndSubmitAnswer(hints)

while (!answerSubmitted) {
  const refreshedTaskResponse = await getTask(tokenResponse.token)
  hints.push(refreshedTaskResponse.hint)
  answerSubmitted = await queryAIAndSubmitAnswer(hints)
}
