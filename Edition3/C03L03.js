import {config} from '../common/config.mjs'

const SOURCE_API = 'https://centrala.ag3nts.org/apidb'

const sendRequest = async (query) => {
  const response = await fetch(SOURCE_API, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      task: 'database',
      apikey: config.apiKey,
      query,
    }),
  })

  return await response.json()
}

const dbSchema = {
  connections: {
    user1_id: 'INT(11)',
    user2_id: 'INT(11)',
  },
  correct_order: {
    base_id: 'INT(11)',
    letter: 'CHAR(1)',
    weight: 'INT(11)',
  },
  datacenters: {
    dc_id: 'INT(11)',
    location: 'VARCHAR(30)',
    manager: 'INT(11)',
    is_active: 'INT(11)',
  },
  users: {
    id: 'INT(11)',
    username: 'VARCHAR(20)',
    access_level: 'VARCHAR(20)',
    is_active: 'INT(11)',
    lastlog: 'DATE'
  },
}

const activeDatacentersWithInactiveUsers = `SELECT datacenters.dc_id, users.username FROM datacenters JOIN users ON datacenters.manager = users.id WHERE datacenters.is_active = 1 AND users.is_active = 0`

sendRequest(activeDatacentersWithInactiveUsers).then((res) => {
  res.reply.forEach((row) => {
    console.log(row)
  })
})

const requestData = {
  task: 'database',
  apikey: config.apiKey,
  answer: [4278, 9294]
}

console.log(requestData)

const verifyResponse = await fetch('https://centrala.ag3nts.org/report', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(requestData),
})

const result = await verifyResponse.json()
console.log('Success:', result)
