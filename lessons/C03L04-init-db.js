import {v4 as uuidv4} from 'uuid'
import {QdrantClient} from '@qdrant/js-client-rest'
import OpenAI from "openai"

const COLLECTION_NAME = "ai_devs_c3l4"

const openai = new OpenAI()
const qdrant = new QdrantClient({url: process.env.QDRANT_URL});
const result = await qdrant.getCollections()
const indexed = result.collections.find((collection) => collection.name === COLLECTION_NAME)
console.log(result)

// Create collection if not exists
if (!indexed) {
  await qdrant.createCollection(COLLECTION_NAME, {vectors: {size: 1536, distance: 'Cosine', on_disk: true}})
}

const collectionInfo = await qdrant.getCollection(COLLECTION_NAME)

// Index documents if not indexed
if (!collectionInfo.points_count) {
  // Read File
  const memoryContent = await fetch('https://unknow.news/archiwum.json')
  const memoryContentJson = await memoryContent.json()

  // Split memory into documents with metadata
  let documents = []

  memoryContentJson.forEach(async (element, index) => {
    if (index > 300) {
      return
    }

    documents.push({
      metadata: {
        source: COLLECTION_NAME,
        content: element.title,
        uuid: uuidv4(),
        url: element.url,
      },
      pageContent: element,
    })
  })

  // Generate embeddings
  const points = []

  for (const document of documents) {
    const embedding = await openai.embeddings.create({
      input: document.metadata.content,
      model: process.env.OPENAI_MODEL_ADA_002
    })

    points.push({
      id: document.metadata.uuid,
      payload: document.metadata,
      vector: embedding.data[0].embedding,
    })
  }

  // Index
  await qdrant.upsert(COLLECTION_NAME, {
    wait: true,
    batch: {
      ids: points.map((point) => (point.id)),
      vectors: points.map((point) => (point.vector)),
      payloads: points.map((point) => (point.payload)),
    },
  })
}
