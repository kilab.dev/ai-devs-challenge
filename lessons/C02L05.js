import {submitAnswer} from "../common/submitAnswer.js"
import {getToken} from "../common/getToken.mjs"

const taskName = "functions"
const tokenResponse = await getToken(taskName)

// Pobranie tokena z odpowiedzi
const token = tokenResponse.token

// Przygotowanie odpowiedzi
const answer = {
  answer: {
    name: 'addUser',
    description: 'Adds a user to the database.',
    parameters: {
      type: 'object',
      properties: {
        name: {
          type: 'string',
          description: 'The name of the user.',
        },
        surname: {
          type: 'string',
          description: 'The surname of the user.',
        },
        year: {
          type: 'number',
          description: 'The year of birth of the user.',
        }
      },
    }
  }
}

// Zgłoszenie odpowiedzi
await submitAnswer(token, answer)
