import OpenAI from 'openai'
import fs from 'fs'
import {config} from '../common/config.mjs'

const openai = new OpenAI()

const robotDescriptionResponse = await fetch(`https://centrala.ag3nts.org/data/${config.apiKey}/robotid.json`).then(res => res.text()).then(res => JSON.parse(res))
const robotDescription = robotDescriptionResponse.description

const imageResponse = await openai.images.generate({
  model: process.env.OPENAI_MODEL_DALL_E,
  prompt: robotDescription,
  n: 1,
  size: "1024x1024",
});

const imageUrl = imageResponse.data[0].url;

// Optional: Download and save the image
const imageResult = await fetch(imageUrl);
const blob = await imageResult.blob();
const arrayBuffer = await blob.arrayBuffer();
const buffer = Buffer.from(arrayBuffer);

fs.writeFileSync('generated-robot.png', buffer);

console.log('Image generated successfully:', imageUrl);

const requestData = {
  task: 'robotid',
  apikey: config.apiKey,
  answer: imageUrl
}

const verifyResponse = await fetch('https://centrala.ag3nts.org/report', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(requestData),
})

const result = await verifyResponse.json()
console.log('Success:', result)
