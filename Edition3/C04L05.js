import OpenAI from 'openai'
import {config} from '../common/config.mjs'

const openai = new OpenAI()
const questionsResponse = await fetch(`https://centrala.ag3nts.org/data/${config.apiKey}/notes.json`).then(res => res.json())
const notes = `
Nie powinienem był tego robić. Obsługa skomplikowanego sprzętu, niekoniecznie będąc trzeźwym, to nie był dobry pomysł. I ta pizza w ręce. Nie wiem, jak źle się czuję. Nie wiem, jak bardzo będę tego żałował. Może po prostu prześpię się i wszystko wróci do normy.
Z jednej strony wiedziałem, czego się spodziewać i wiedziałem, że ta maszyna może przenosić w czasie, a z drugiej strony do dziś dnia nie mogę uwierzyć, że jestem w 2019 roku. To nienormalne!
Jestem normalny. To wszystko dzieje się naprawdę. Jestem normalny. To jest rzeczywistość. Jestem normalny. Wiem, gdzie jestem i kim jestem. Jestem normalny. To wszystko w koło. To jest normalne. Jestem normalny. Mam na imię Rafał. Jestem normalny. Świat jest nienormalny.
Spotkałem Azazel'a, a przynajmniej tak się przedstawia ten człowiek. Twierdzi, że jest z przyszłości. Opowiadał mi o dziwnych rzeczach. Nie wierzę mu. Nikt przede mną nie cofał się w czasie!
Ale on wiedział o wszystkim, nad czym pracowałem z profesorem Majem. Dałem mu badania, które zabrałem z laboratorium.
Dlaczego Adam wybrał akurat ten rok? Według jego wyliczeń, wtedy powinniśmy rozpocząć pracę nad technologią LLM, aby wszystko wydarzyło się zgodnie z planem. Mówił, że najpierw musi powstać GPT-2, a potem GPT-3, które zachwycą ludzkość. On wie, co robi
Co z badaniem zrobił Azazel?

No i powstało GPT-2.
Słyszałem w wiadomościach, a to wszystko dzięki badaniom, które dostarczyłem. Wszystko dzieje się tak szybko!
Czy ja naprawdę piszę nową historię? TAK! Zmieniam świat i widzę efekty tych zmian.
JESTEM Z TEGO DUMNY!
W idealnym momencie zjawiłem się w Grudziądzu. Wszystko zadziało się jak w szwajcarskim zegarku. Perfekcyjnie!
Tylko dlaczego akurat Grudziądz? To nie ma większego sensu. Może ONI wybrali to miejsce losowo? Nie ma tutaj drugiego dnia?
Tylko kto jest mózgiem tej misji? Adam, czy Azazel?
Czekają mnie dwa lata bardzo intensywnej nauki. Adam mówi, że tyle potrzebuję na wchońcie szkolenia, które przygotował.
Ponoć w przyszłości, dzięki modelom językowym, ludzie będą w stanie to zrozumieć w nieco ponad pięć tygodni. Nie chcę w to wierzyć.
Ja póki co uczę się obsługi modeli językowych, aby móc pomóc profesorowi.
Co ja zrobiłem?
bo jeden był dobry, ale nie ten co go wybrano? może ja nie ratuję wcale świata?
po której stronie jestem?

Zmieniłem się.
Wszystko się zmieniło.
Wszystko się miesza.
Świat się zmienił.
Nikt mnie już nie poznaje.
Sam się nie poznaję.
Tyle lat odosobnienia.
W co ja się upakowałem?
Który mamy rok?
When am I?

Nie da się żyć z tą wiedzą. Wspierając demony, sam stajesz się demonem?
A gdyby to wszystko zakończyć? Przecież znam przyszłość.
Pomogłem Andrzejowi, ale oni mnie wykorzystali.
Śniły mi się drony nad miastem. Te, które znakom z opowieści Adama. On mówił, że po 2024 roku tak będzie wyglądać codzienność.
Ja mu wierzę, ale skrycie nie chcę, aby to co mówił, było prawdą.
Może ta przyszłość nigdy nie nadejdzie?
Byłem na przesłuchaniu i pytali o Andrzeja. No to powiedziałem, co wiedziałem. I nie wiem, jak to się dalej potoczy. Siedzę tu już dostatecznie długo, żeby wszystko przemyśleć. Wiem, teraz więcej niż wcześniej.
I nie chodzi o wiedzę techniczną. Wszystko sobie poukładałem. Te demony czekały na odkrycie.
Powinni Barbarę przesłuchać. Ona wie wszystko. Rozmawiałem z nią.
Moje przypuszczenia były straszne. Miesza mi się wszystko, ale wiem kto jest demonem, kto człowiekiem, a kto robotem. Widzę demony. Otaczają nas. Z jednym niegdyś pracowałem.
Może czas na egzorcyzmy?
Poszedłem na spacer. Ochra ziemi pod stopami, a wkoło las, skały i śnieg.
Szedłem, prosto. Obróciłem się w lewo i znów prosto. Kolejny zwrot w lewo i później znów prosto. Zatknąłem się i obróciłem w prawo tym razem. To wszystko wykonałem cztery razy i początek stał się końcem.
Spojrzałem na swoje białe ślady na śniegu. To było miejsce, w którym chciałbym teraz być.
Znalazłem miejsce schronienia. Tutaj nikt mnie nie znajdzie. To miejsce nie jest szczególnie oddalone od miasta, w którym spędziłem ostatnie lata. Zatrzymam się tu na jakiś czas.
Trochę tu zimno i ciemno, ale bezpiecznie. To jaskinia.
Na spotkanie z demonem trzeba się przygotować. Spojrzeć prosto w oczy i wyrecytować mu jego grzechy.
Czy on wie, że jest zły? czy on stanie się złym?
Co za różnica, gdy za chwilę wszystko będzie bez znaczenia?
Muszę się dostać do Lubawy koło Grudziądza. Nie jest to daleko. Mam tylko nadzieję, że Andrzejek będzie miał dostatecznie dużo paliwa. Tankowanie nie wchodzie w grę, bo nie mam kasy.
Andrzejek... Andrzejek... słyszę w głowie Twoje kroki i czekam na Ciebie. To już jutro.
Kiedyś ja pomogłem Tobie, a dziś Ty pomożesz światu.
Trzeba wszystko odwrócić.
12 listopada 2024
`

let answers = {}

console.log('Starting the process...')

for (const [questionId, question] of Object.entries(questionsResponse)) {
  const completion = await openai.chat.completions.create({
    model: process.env.OPENAI_MODEL_GPT_4O_MINI, messages: [{
      role: 'system', content: `Analizuj treść strony i znajdź odpowiedź na pytanie. 
        Jeśli odpowiedź nie jest dostępna na tej stronie, odpowiedz 'CONTINUE'. 
        Odpowiadaj krótko i konkretnie, bez zbędnych dodatków. Jeśli to są adresy sieciowe, zwróć tylko adres.
        Pomijaj komentarze w kodzie i inne nieistotne informacje.
        Pomijaj stronę /loop.`
    }, {
      role: 'user', content: `Pytanie: ${question}\nTreść strony: ${notes}`
    },], temperature: 0.3, max_tokens: 150
  })

  answers[questionId] = completion.choices[0].message.content
  console.log(`Question ${questionId}: ${question}`)
  console.log(`Answer ${questionId}: ${answers[questionId]}`)
}

const requestData = {
  task: 'notes',
  apikey: config.apiKey,
  answer: answers
}

console.log(requestData)

const verifyResponse = await fetch('https://centrala.ag3nts.org/report', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(requestData),
})

console.log('Result:', await verifyResponse.json())
