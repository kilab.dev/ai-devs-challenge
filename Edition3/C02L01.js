import OpenAI from 'openai'
import fs from 'fs'
import path from 'path'
import {config} from '../common/config.mjs'

const openai = new OpenAI()
const CACHE_DIR = path.join(process.cwd(), 'cache', 'transcriptions')

// Ensure cache directory exists
if (!fs.existsSync(CACHE_DIR)) {
  fs.mkdirSync(CACHE_DIR, { recursive: true })
}

const audioFiles = [
  { name: 'Adam', file: 'adam.m4a' },
  { name: 'Agnieszka', file: 'agnieszka.m4a' },
  { name: 'Ardian', file: 'ardian.m4a' },
  { name: 'Michał', file: 'michal.m4a' },
  { name: 'Monika', file: 'monika.m4a' },
  { name: 'Rafał', file: 'rafal.m4a' }
]

const transcriptionPromises = audioFiles.map(async ({ name, file }) => {
  const cacheFile = path.join(CACHE_DIR, `${file}.json`)

  if (fs.existsSync(cacheFile)) {
    console.log(`Loading ${name}'s transcription from cache...`)
    const cached = JSON.parse(fs.readFileSync(cacheFile, 'utf-8'))
    return { name, response: cached }
  }

  console.log(`Transcribing ${name}'s audio...`)
  const response = await openai.audio.transcriptions.create({
    file: fs.createReadStream(`${process.cwd()}/Edition3/res/C02L01/${file}`),
    model: process.env.OPENAI_MODEL_WHISPER
  })

  // Save to cache
  fs.writeFileSync(cacheFile, JSON.stringify(response))
  return { name, response }
})

const results = await Promise.all(transcriptionPromises)

const transcriptions = Object.fromEntries(
  results.map(({ name, response }) => [name.toLowerCase(), response])
)

const testimonies = Object.entries(transcriptions).map(([name, response]) => {
  return `${name}: ${response.text}`
}).join('\n\n')

console.log('Analyzing testimonies...')
const chatCompletion = await openai.chat.completions.create({
  messages: [
    {
      role: 'system',
      content: `
        As an detective, You have to find the name of the street where the university where Andrzej Maj teaches is located.
        As context, you will receive the testimony of several witnesses from the user.
        Remember that witness statements may be contradictory, some may be wrong, and others may answer in rather bizarre ways.
        Analyze the testimonies and find the name of the street where the university is located. 
        Take into account the street of the main university building (headquarters).
        At the end of your response write the name of the street without number in the answer in Polish in format: "ANSWER: ul. [street name]".
      `,
    },
    {
      role: 'user',
      content: testimonies,
    },
  ],
  model: process.env.OPENAI_MODEL_GPT_4,
})

console.log(chatCompletion.choices[0].message.content)

const streetName = chatCompletion.choices[0].message.content.match(/ANSWER: ul\. ([\w\s]+)/)[1]

console.log(`Street name: ${streetName}`)

const requestData = {
  task: 'mp3',
  apikey: config.apiKey,
  answer: streetName
}

const verifyResponse = await fetch('https://centrala.ag3nts.org/report', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(requestData),
})

const result = await verifyResponse.json()
console.log('Success:', result)
