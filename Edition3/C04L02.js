import OpenAI from 'openai'
import {config} from '../common/config.mjs'
import fs from 'fs'
import * as tf from '@tensorflow/tfjs-node'

// const openai = new OpenAI()

const correctSeries = fs.readFileSync(`${process.cwd()}/res/C04L02/correct.txt`, 'utf8')
  .trim()
  .split('\n')
  .filter(Boolean)
const incorrectSeries = fs.readFileSync(`${process.cwd()}/res/C04L02/correct.txt`, 'utf8')
  .trim()
  .split('\n')
  .filter(Boolean)
const seriesToVerify = fs.readFileSync(`${process.cwd()}/res/C04L02/verify.txt`, 'utf8')
  .trim()
  .split('\n')
  .filter(Boolean)
  .map((series) => series.split('='))

const trainingData = [
  ...correctSeries.map(series => ({
    messages: [
      { role: "system", content: "You are an AI that classifies number sequences as correct or incorrect." },
      { role: "user", content: series },
      { role: "assistant", content: "correct" }
    ]
  })),
  ...incorrectSeries.map(series => ({
    messages: [
      { role: "system", content: "You are an AI that classifies number sequences as correct or incorrect." },
      { role: "user", content: series },
      { role: "assistant", content: "incorrect" }
    ]
  }))
]

fs.writeFileSync('../cache/c04l02_training_data.jsonl',
  trainingData.map(item => JSON.stringify(item)).join('\n')
)

async function trainAndClassify() {
  try {
    const file = await openai.files.create({
      file: fs.createReadStream('../cache/c04l02_training_data.jsonl'),
      purpose: 'fine-tune'
    })

    console.log('File uploaded:', file.id)

    const fineTune = await openai.fineTuning.jobs.create({
      training_file: file.id,
      model: 'gpt-3.5-turbo-0125'
    })

    console.log('Fine-tuning job created:', fineTune.id)

    let fineTuneStatus = await openai.fineTuning.jobs.retrieve(fineTune.id)
    while (fineTuneStatus.status !== 'succeeded') {
      await new Promise(resolve => setTimeout(resolve, 5000))
      fineTuneStatus = await openai.fineTuning.jobs.retrieve(fineTune.id)
      console.log('Current status:', fineTuneStatus.status)
    }

    const results = {}
    for (const [series, expected] of seriesToVerify) {
      const completion = await openai.chat.completions.create({
        model: fineTuneStatus.fine_tuned_model,
        messages: [
          { role: "system", content: "You are an AI that classifies number sequences as correct or incorrect." },
          { role: "user", content: series }
        ]
      })

      const prediction = completion.choices[0].message.content.trim()
      results[series] = {
        predicted: prediction,
        expected: expected
      }
    }

    return results
  } catch (error) {
    console.error('Error:', error)
    throw error
  }
}

async function classify() {
  try {
    const results = {}
    for (const [series, expected] of seriesToVerify) {
      const completion = await openai.chat.completions.create({
        model: 'ft:gpt-4o-mini-2024-07-18:grupa-morizon-gratka:weryfikuj-dane-kgendig:AXuz83ZG',
        messages: [
          { role: "system", content: "You are an AI that classifies number sequences as correct or incorrect." },
          { role: "user", content: series }
        ]
      })

      const prediction = completion.choices[0].message.content.trim()
      results[series] = {
        predicted: prediction,
        expected: expected
      }
    }

    return results
  } catch (error) {
    console.error('Error:', error)
    throw error
  }
}

// trainAndClassify().then(results => {
//   console.log('Classification results:', results)
// }).catch(error => {
//   console.error('Error in main process:', error)
// })

// classify().then(results => {
//   console.log('Classification results:', results)
// }).catch(error => {
//   console.error('Error in main process:', error)
// })

function validateAndConvertSeriesTF(series) {
  if (!Array.isArray(series)) return false;
  if (series.length !== 4) return false;
  return series.every(num => !isNaN(parseFloat(num)));
}

function processDataTF(data) {
  return data
    .map(line => line.split(',').map(num => parseFloat(num)))
    .filter(validateAndConvertSeriesTF);
}

const trainingDataTF = [
  ...processDataTF(correctSeries),
  ...processDataTF(incorrectSeries)
]

async function runClassification() {
  try {
    const correctSeries = fs.readFileSync(`${process.cwd()}/res/C04L02/correct.txt`, 'utf8')
      .trim()
      .split('\n')
      .filter(Boolean);

    const incorrectSeries = fs.readFileSync(`${process.cwd()}/res/C04L02/incorrect.txt`, 'utf8')
      .trim()
      .split('\n')
      .filter(Boolean);

    const trainingData = [
      ...processDataTF(correctSeries),
      ...processDataTF(incorrectSeries)
    ];

    if (trainingData.length === 0) {
      throw new Error('No valid training data found');
    }

    const correctCount = processDataTF(correctSeries).length;
    const incorrectCount = processDataTF(incorrectSeries).length;

    const labels = [
      ...Array(correctCount).fill(1),
      ...Array(incorrectCount).fill(0)
    ];

    const xTrain = tf.tensor2d(trainingData);
    const yTrain = tf.tensor1d(labels);

    const model = tf.sequential();
    model.add(tf.layers.dense({
      units: 8,
      activation: 'relu',
      inputShape: [4]
    }));
    model.add(tf.layers.dense({
      units: 1,
      activation: 'sigmoid'
    }));

    model.compile({
      optimizer: tf.train.adam(0.01),
      loss: 'binaryCrossentropy',
      metrics: ['accuracy']
    });

    await model.fit(xTrain, yTrain, {
      epochs: 100,
      validationSplit: 0.2,
      verbose: 1
    });

    const verifyData = fs.readFileSync(`${process.cwd()}/res/C04L02/verify.txt`, 'utf8')
      .trim()
      .split('\n')
      .filter(Boolean)
      .map(line => {
        const [id, seriesStr] = line.split('=');
        const series = seriesStr.split(',').map(num => parseFloat(num));
        return {
          id: id.trim(),
          data: series
        };
      })
      .filter(item => validateAndConvertSeriesTF(item.data));

    const xVerify = tf.tensor2d(verifyData.map(item => item.data));
    const predictions = await model.predict(xVerify).array();

    verifyData.forEach((item, i) => {
      console.log(`Line ${item.id}: ${predictions[i][0] > 0.5 ? 'correct' : 'incorrect'} (${predictions[i][0].toFixed(4)})`);
    });

    xTrain.dispose();
    yTrain.dispose();
    xVerify.dispose();
    model.dispose();

  } catch (error) {
    console.error('Error during classification:', error);
  }
}

// Uruchomienie klasyfikacji
// runClassification().then(() => {
//   console.log('Classification completed');
// })

console.log('========')

const requestData = {
  task: 'research',
  apikey: config.apiKey,
  answer: [
    '01',
    '02',
    '10'
  ]
}

console.log(requestData)

const verifyResponse = await fetch('https://centrala.ag3nts.org/report', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(requestData),
})

const result = await verifyResponse.json()
console.log('Success:', result)
