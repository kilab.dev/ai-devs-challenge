import {config} from '../common/config.mjs'

async function fetchDataAndVerify() {
  try {
    const requestData = {
      task: 'POLIGON',
      apikey: config.apiKey,
      answer: []
    }

    const response = await fetch('https://poligon.aidevs.pl/dane.txt')
    const text = await response.text()
    requestData.answer = text.split('\n').filter(line => line.trim())

    const verifyResponse = await fetch('https://poligon.aidevs.pl/verify', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestData),
    })

    const result = await verifyResponse.json()
    console.log('Success:', result)
  } catch (error) {
    console.error('Error:', error)
  }
}

fetchDataAndVerify()
