import {config} from "./config.mjs";

export async function getTask(token) {
  const url = `${config.taskManagerUrl}/task/${token}`;
  const response = await fetch(url);

  return await response.json();
}
