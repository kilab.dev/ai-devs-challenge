import OpenAI from "openai"
import {submitAnswer} from "../common/submitAnswer.js"
import {getToken} from "../common/getToken.mjs"
import {config} from "../common/config.mjs"

const openai = new OpenAI()

const taskName = "liar"
const tokenResponse = await getToken(taskName)

// Pobranie tokena z odpowiedzi
const token = tokenResponse.token

const question = 'What is your name?'
const questionResponse = await fetch(`${config.taskManagerUrl}/task/${tokenResponse.token}`, {
  body: `question=${question}`,
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
  },
  method: 'POST',
})

const taskAnswer = JSON.parse(await questionResponse.text()).answer

const chatCompletion = await openai.chat.completions.create({
  messages: [
    {
      role: 'system',
      content: `
        As a moderator, you have to recognize if the answer to the question is to the point.
        User sends you a message: question: answer.
        If the answer to the question is to the point, you should respond with: "YES".
        Otherwise, you should respond with: "NO".
      `,
    },
    {
      role: 'user',
      content: `${question}: ${taskAnswer}`,
    },
  ],
  model: process.env.OPENAI_MODEL_GPT_4,
})

// Przygotowanie odpowiedzi
const answer = { answer: chatCompletion.choices[0].message.content }

// Zgłoszenie odpowiedzi
await submitAnswer(token, answer)
