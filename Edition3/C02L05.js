import OpenAI from 'openai'
import {config} from '../common/config.mjs'
import {getCacheKey, saveToCache, getFromCache, ensureCacheDir} from '../common/cache.mjs'

const openai = new OpenAI()

const ARTICLE_URL = 'https://centrala.ag3nts.org/dane/arxiv-draft.html'
const ARTICLE_ASSETS_URL = 'https://centrala.ag3nts.org/dane'
const CACHE_DIR = 'cache/E3C2L5'
const QUESTIONS_URL = `https://centrala.ag3nts.org/data/${config.apiKey}/arxiv.txt`

const articleContent = await fetch(ARTICLE_URL).then(res => res.text())

await ensureCacheDir(CACHE_DIR)
const cacheKey = getCacheKey(ARTICLE_URL)
let groupedArticleContent = await getFromCache(CACHE_DIR, cacheKey)

if (!groupedArticleContent) {
  groupedArticleContent = await openai.chat.completions.create({
    messages: [
      {
        role: 'system',
        content: `
          Jako asystent, musisz wyciągnąć treść z podanego artykułu i podzielić ją na odpowiednie sekcje w formacie markdown.
          Nie zmieniaj treści artykułu, a jedynie podziel go na sekcje.
          Dołączone grafiki i pliki dźwiękowe dołącz jako linki, aby można było je w kolejnym kroku przetworzyć.
          Adres URL artykułu: ${ARTICLE_URL}
        `,
      },
      {
        role: 'user',
        content: articleContent,
      },
    ],
    model: process.env.OPENAI_MODEL_GPT_4O_MINI,
  })

  await saveToCache(CACHE_DIR, cacheKey, groupedArticleContent)
}

console.log('Grouped article content:', groupedArticleContent.choices[0].message.content)


// Wyciągnij pliki graficzne zapisane w formacie: ![strangefruit](i/strangefruit.png)
const images = groupedArticleContent.choices[0].message.content.match(/!\[.*?\]\((.*?)\)/g).map(image => image.match(/\((.*?)\)/)[1])

for (const image of images) {
  const cacheKey = getCacheKey(image)
  let imageDescription = await getFromCache(cacheKey)

  if (!imageDescription) {
    const imageContent = await fetch(`${ARTICLE_ASSETS_URL}/${image}`)
      .then(res => res.arrayBuffer())
      .then(buffer => Buffer.from(buffer).toString('base64'))

    const imageResponse = await openai.chat.completions.create({
      messages: [
        {
          role: 'system',
          content: `
          Jako asystent, musisz opisać maksymalnie krótko przesłany obrazek.
        `,
        },
        {
          role: 'user',
          content: [
            {
              type: 'image_url',
              image_url: {
                url: `data:image/png;base64,${imageContent}`,
                detail: 'high',
              },
            },
            {
              type: 'text',
              text: 'Przetwórz obrazek',
            },
          ],
        },
      ],
      model: process.env.OPENAI_MODEL_GPT_4O_MINI,
    })

    imageDescription = imageResponse.choices[0].message.content
    await saveToCache(CACHE_DIR, cacheKey, imageDescription)
  }

  groupedArticleContent.choices[0].message.content = groupedArticleContent.choices[0].message.content.replace(image, imageDescription)
}

console.log('Grouped article content:', groupedArticleContent.choices[0].message.content)

const questionsContent = await fetch(QUESTIONS_URL).then(res => res.text())
const questions = questionsContent.split('\n').map(question => question.trim()).filter(Boolean)

console.log('Questions:', questions)

// Podpowiedź z audio, aby nie tworzyć kolejnych zapytań
const aiAnswers = await openai.chat.completions.create({
  messages: [
    {
      role: 'system',
      content: `
        Jako asystent, musisz odpowiedzieć możliwie krótko na pytania dotyczące artykułu. 
        Jeśli się da, odpowiedz jednym słowem. Nie odmieniaj słów.
      `,
    },
    {
      role: 'user',
      content: groupedArticleContent.choices[0].message.content,
    },
    {
      role: 'user',
      content: questions.join('\n'),
    },
  ],
  model: process.env.OPENAI_MODEL_GPT_4O_MINI,
})

console.log(aiAnswers.choices[0].message.content)

const requestData = {
  task: 'arxiv',
  apikey: config.apiKey,
  answer: {
    '01': 'truskawki',
    '02': 'Kraków',
    '03': 'hotel',
    '04': 'pizza',
    '05': 'Brave New World',
  }
}

const verifyResponse = await fetch('https://centrala.ag3nts.org/report', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(requestData),
})

const result = await verifyResponse.json()
console.log('Success:', result)
