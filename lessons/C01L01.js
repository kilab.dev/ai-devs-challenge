import {submitAnswer} from "../common/submitAnswer.js"
import {getToken} from "../common/getToken.mjs"
import {getTask} from "../common/getTask.mjs"

const taskName = "helloapi"
const tokenResponse = await getToken(taskName)
const taskResponse = await getTask(tokenResponse.token)

// Pobranie tokena z odpowiedzi
const token = tokenResponse.token

// Pobranie zmiennej cookie z zadania
const cookie = taskResponse.cookie

// Przygotowanie odpowiedzi
const answer = {answer: cookie}

// Zgłoszenie odpowiedzi
await submitAnswer(token, answer)
