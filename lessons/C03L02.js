import {submitAnswer} from "../common/submitAnswer.js"
import {getToken} from "../common/getToken.mjs"
import {getTask} from "../common/getTask.mjs";
import OpenAI from "openai";

const openai = new OpenAI()

const taskName = "scraper"
const tokenResponse = await getToken(taskName)
const taskResponse = await getTask(tokenResponse.token)

// Pobranie tokena z odpowiedzi
const token = tokenResponse.token

const inputDataUrl = taskResponse.input
const question = taskResponse.question

// Pobranie danych z artykułu
function getArticleContent(url) {
  return new Promise((resolve) => {
    fetch(url, {
      headers: {
        'Content-Type': 'text/html',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'
      }
    })
      .then(async response => {
        if (response.status === 200) {
          resolve(await response.text())
        } else {
          setTimeout(() => {
            getArticleContent(url)
          }, 2000)
        }
      })
      .catch(() => {
        getArticleContent(url)
      })
  })
}

const articleContent = await getArticleContent(inputDataUrl)

// Odpytanie AI
const chatCompletion = await openai.chat.completions.create({
  messages: [
    {
      role: 'system',
      content: `
        Return answer for the question in POLISH language, based on provided article. Maximum length for the answer is 200 characters
      `,
    },
    {
      role: 'user',
      content:
      `
      Article: ${articleContent}
      
      Question: ${question}
      `,
    },
  ],
  model: process.env.OPENAI_MODEL_GPT_4,
})

// Przygotowanie odpowiedzi
const answer = {answer: chatCompletion.choices[0].message.content}

// Zgłoszenie odpowiedzi
await submitAnswer(token, answer)
