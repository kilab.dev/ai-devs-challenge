import OpenAI from 'openai'
import fs from 'fs/promises'
import {ensureCacheDir, getFromCache, saveToCache} from '../common/cache.mjs'
import {config} from '../common/config.mjs'

const CACHE_DIR = 'cache/E3C3L1'
const SOURCE_DATA_DIRECTORY_PATH = 'Edition3/res/C03L01'
const openai = new OpenAI()

await ensureCacheDir(CACHE_DIR)

const factsSummary = await getFromCache(CACHE_DIR, 'factsSummary.json')

if (!factsSummary) {
  const sourceFactFiles = await fs.readdir(`${SOURCE_DATA_DIRECTORY_PATH}/facts`)
  const facts = []

  for (const factFile of sourceFactFiles) {
    const filePath = `${SOURCE_DATA_DIRECTORY_PATH}/facts/${factFile}`
    const extension = factFile.split('.').pop()

    switch (extension) {
      case 'txt':
        const fileContent = await fs.readFile(filePath, 'utf-8')

        if (fileContent.startsWith('entry deleted')) {
          continue
        }

        const singleFacts = fileContent.split('\n').filter(Boolean)

        for (const fact of singleFacts) {
          facts.push(fact)
        }
        break
      default:
        console.log('Unsupported file:', filePath)
    }
  }

  const factsSummaryResponse = await openai.chat.completions.create({
    model: process.env.OPENAI_MODEL_GPT_4O_MINI,
    messages: [
      {
        role: 'system',
        content: `Stwórz podsumowanie faktów odnośnie każdej z osób.
        Odpowiedź zwróć jako plik JSON: { "person": "podsumowanie", "person2": "podsumowanie2", ... } bez żadnych dodatkowych informacji, abym mógł go łatwo sparsować.
      `
      },
      ...facts.map(fact => ({role: 'user', content: fact.split(' - ')[0]}))
    ]
  })

  const factsSummary = JSON.parse(
    factsSummaryResponse.choices[0].message.content
      .replace('```json\n', '')
      .replace('```', '')
  )

  await saveToCache(CACHE_DIR, 'factsSummary.json', factsSummary)
}


const sourceReportFiles = await fs.readdir(`${SOURCE_DATA_DIRECTORY_PATH}/reports`)
const reportsKeywords = {}

for (const reportFile of sourceReportFiles) {
  const reportKeywords = await getFromCache(CACHE_DIR, reportFile + '.json')

  if (!reportKeywords) {
    const filePath = `${SOURCE_DATA_DIRECTORY_PATH}/reports/${reportFile}`
    const extension = reportFile.split('.').pop()

    switch (extension) {
      case 'txt':
        const reportContent = await fs.readFile(filePath, 'utf-8')
        let personFactsSummary = null

        for (const [key, value] of Object.entries(factsSummary)) {
          if (reportContent.includes(key)) {
            personFactsSummary = value
          }
        }

        let query = ''

        if (!personFactsSummary) {
          query = `Podsumowanie dla osoby w raporcie: ${reportContent}`
        } else {
          query = `Podsumowanie dla osoby w raporcie: ${personFactsSummary}\n${reportContent}`
        }

        const summaryResponse = await openai.chat.completions.create({
          model: process.env.OPENAI_MODEL_GPT_4O,
          messages: [
            {
              role: 'system',
              content: `Wygeneruj słowa kluczowe w formie mianownika w liczbie pojedynczej (czyli np. “sportowiec”, a nie “sportowcem”, “sportowców” itp.) dla wskazanego raportu o tym co się wydarzyło, w jakim miejscu i kogo, lub czego dotyczy.
              Nie używaj imion i nazwisk innych osób, ani miejsc, które nie są wymienione w raporcie.
              Odpowiedź zwróć jako plik JSON: ["słowo1", "słowo2", ...].
              `
            },
            {
              role: 'user',
              content: query
            }
          ]
        })

        console.log(summaryResponse.choices[0].message.content)

        const keywords = JSON.parse(
          summaryResponse.choices[0].message.content
            .replace('```json\n', '')
            .replace('```', '')
        )
        await saveToCache(CACHE_DIR, reportFile + '.json', keywords)
        reportsKeywords[reportFile] = keywords.join(', ')
        break
      default:
        console.log('Unsupported file:', filePath)
    }
  } else {
    reportsKeywords[reportFile] = reportKeywords.join(', ')
  }
}

const requestData = {
  task: 'dokumenty',
  apikey: config.apiKey,
  answer: reportsKeywords
}

console.log(requestData)

const verifyResponse = await fetch('https://centrala.ag3nts.org/report', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(requestData),
})

const result = await verifyResponse.json()
console.log('Success:', result)
