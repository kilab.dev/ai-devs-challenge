# AI Devs challenge - Node.js based solution

## Requirements
- Node.js 20.0.0 or higher

## How to run
- Clone this repository
- Set API key in `.env` file based on `.env.dist` file (you can get API key from https://zadania.aidevs.pl/)
- Set OpenAI API key in `.env` file (you can get API key from https://platform.openai.com/account/api-keys)
- Run `node --env-file .env lessons/LESSON_FILE.js`
