import OpenAI from 'openai'
import fs from 'fs'
import path from 'path'

import {config} from '../common/config.mjs'

const openai = new OpenAI()

const __dirname = path.resolve(path.dirname(''))
const inputFilePath = path.join(__dirname, 'Edition3', 'res', 'C01L03.json')

async function main() {
  const inputData = JSON.parse(await fs.promises.readFile(inputFilePath, 'utf8'))

  inputData.apikey = process.env.API_KEY

  for (const data of inputData['test-data']) {
    const question = data.question.split(' ')

    if (question.length === 3 && question[1] === '+') {
      data.answer = parseInt(question[0]) + parseInt(question[2])
    } else {
      console.error('Invalid question:', data.question)
    }

    if (data.test !== undefined) {
      const chatCompletion = await openai.chat.completions.create({
        messages: [
          {
            role: 'system',
            content: `
          As a assistant, You have to respond with valid answer to the question asked by the user.
          You have to respond with single word in the answer.
        `,
          },
          {
            role: 'user',
            content: data.test.q,
          },
        ],
        model: process.env.OPENAI_MODEL_GPT_4O_MINI,
      })

      console.log('AI question:', data.test.q)
      console.log('AI response:', chatCompletion.choices[0].message.content)
      data.test.a = chatCompletion.choices[0].message.content
    }
  }

  const requestData = {
    task: 'JSON',
    apikey: config.apiKey,
    answer: inputData,
  }

  const verifyResponse = await fetch('https://centrala.ag3nts.org/report', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(requestData),
  })

  const result = await verifyResponse.json()
  console.log('Success:', result)
}

main().catch(console.error)
