import {submitAnswer} from "../common/submitAnswer.js"
import {getToken} from "../common/getToken.mjs"

const taskName = "ownapipro"
const tokenResponse = await getToken(taskName)

// Pobranie tokena z odpowiedzi
const token = tokenResponse.token

// Stworzenie odpowiedzi
const answer = { answer: 'https://openai.kilab.dev/' }

// Zgłoszenie odpowiedzi
await submitAnswer(token, answer)
