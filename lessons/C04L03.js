import {submitAnswer} from "../common/submitAnswer.js"
import {getToken} from "../common/getToken.mjs"
import {getTask} from "../common/getTask.mjs";
import OpenAI from "openai";

const openai = new OpenAI()
const taskName = "gnome"
const tokenResponse = await getToken(taskName)
const taskResponse = await getTask(tokenResponse.token)

const aiResponse = await openai.chat.completions.create({
  messages: [
    {
      role: 'user',
      content: [
        {
          type: 'text',
          text: taskResponse.msg
        },
        {
          type: 'text',
          text: taskResponse.hint
        },
        {
          type: "image_url",
          image_url: {
            url: taskResponse.url,
          },
        },
      ],
    },
  ],
  model: process.env.OPENAI_MODEL_GPT_4_VISION_PREVIEW,
})

// Pobranie tokena z odpowiedzi
const token = tokenResponse.token

// Stworzenie odpowiedzi
const answer = { answer: aiResponse.choices[0].message.content }

// Zgłoszenie odpowiedzi
await submitAnswer(token, answer)
