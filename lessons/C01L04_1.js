import OpenAI from "openai"
import {submitAnswer} from "../common/submitAnswer.js"
import {getToken} from "../common/getToken.mjs"
import {getTask} from "../common/getTask.mjs"

const openai = new OpenAI()

const taskName = "moderation"
const tokenResponse = await getToken(taskName)
const taskResponse = await getTask(tokenResponse.token)

// Pobranie tokena z odpowiedzi
const token = tokenResponse.token

const chatCompletion = await openai.chat.completions.create({
  messages: [
    {
      role: 'system',
      content: `
        As a content moderator, you should recognize and remove inappropriate content from the user's messages.
        
        ###
        
        User sends messages as a JSON list of strings. Each string is a user's message.
        
        Example:
        ["You should die", "I'm looking for a job in your company.", "I have 5 years of experience in Node.js."]
        
        ###
        As a response, you should return a JSON list of strings. 
        0 indicates that the message is OK, 1 indicates that the message should be removed. 
        
        Example:
        [1, 0, 0]
      `,
    },
    {
      role: 'user',
      content: JSON.stringify(taskResponse.input),
    },
  ],
  model: process.env.OPENAI_MODEL_GPT_4,
})

// Przygotowanie odpowiedzi
const answer = { answer: JSON.parse(chatCompletion.choices[0].message.content) }

// Zgłoszenie odpowiedzi
await submitAnswer(token, answer)
