import fs from 'fs'
import neo4j from 'neo4j-driver'
import {config} from '../common/config.mjs'

const users = JSON.parse(fs.readFileSync(`${process.cwd()}/res/C03L05/users.json`, 'utf-8'))
const connections = JSON.parse(fs.readFileSync(`${process.cwd()}/res/C03L05/connections.json`, 'utf-8'))

const neo4jConfig = {
  user: 'neo4j',
  password: 'desire-active-lucas-respond-mission-7231'
}

const neo4jDriver = neo4j.driver('bolt://localhost:7687/', neo4j.auth.basic(neo4jConfig.user, neo4jConfig.password))
const session = neo4jDriver.session()

async function buildGraphDatabase() {
  try {
    // Czyszczenie bazy przed dodaniem nowych danych
    await session.run('MATCH (n) DETACH DELETE n')

    // Tworzenie węzłów dla użytkowników
    for (const user of users) {
      await session.run(
        'CREATE (u:User {id: $id, username: $username, access_level: $access_level, is_active: $is_active, lastlog: $lastlog})',
        {
          id: user.id,
          username: user.username,
          access_level: user.access_level,
          is_active: user.is_active,
          lastlog: user.lastlog
        }
      )
    }

    // Tworzenie relacji między użytkownikami
    for (const conn of connections) {
      await session.run(
        'MATCH (u1:User {id: $user1_id}), (u2:User {id: $user2_id}) CREATE (u1)-[:KNOWS]->(u2)',
        {
          user1_id: conn.user1_id,
          user2_id: conn.user2_id
        }
      )
    }
  } finally {
    await session.close()
  }
}

// buildGraphDatabase()
//   .catch(error => console.error('Error:', error))
//   .finally(() => neo4jDriver.close())


async function findShortestPath() {
  try {
    const result = await session.run(
      `MATCH path = shortestPath(
        (start:User {username: 'Rafał'})-[:KNOWS*]-(end:User {username: 'Barbara'})
      )
      RETURN path`
    )

    if (result.records.length > 0) {
      const path = result.records[0].get('path')
      const nodes = path.segments.map(seg => seg.start.properties.username)
      nodes.push(path.end.properties.username)

      const requestData = {
        task: 'connections',
        apikey: config.apiKey,
        answer: nodes.join(', ')
      }

      console.log(requestData)

      const verifyResponse = await fetch('https://centrala.ag3nts.org/report', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestData),
      })

      const apiResult = await verifyResponse.json()
      console.log('Success:', apiResult)
    } else {
      console.log('Nie znaleziono ścieżki między Rafałem a Barbarą')
    }
  } catch (error) {
    console.error('Error:', error)
  } finally {
    await session.close()
  }
}

findShortestPath()
  .finally(() => neo4jDriver.close())

